const criptomonedasS = document.querySelector('#criptomonedas');
const monedasS = document.querySelector('#moneda');
const formulario = document.querySelector('#formulario');
const resultado = document.querySelector('#resultado');

const busqueda = {
  moneda: '',
  criptomoneda: ''
}



const obtenerCriptomonedas = criptomonedas => new Promise(resolve => {
  resolve(criptomonedas);
});


document.addEventListener('DOMContentLoaded', main);

function main() {
  consultarCriptomonedas();
  eventsListener();
}
function eventsListener() {
  formulario.addEventListener('submit', validarFormulario);
  criptomonedasS.addEventListener('change', obtenerValorInput);
  monedasS.addEventListener('change', obtenerValorInput);
}

async function consultarCriptomonedas() {
  const URI = 'https://min-api.cryptocompare.com/data/top/mktcapfull?limit=10&tsym=USD';

  try {
    const response = await fetch(URI);
    const data = await response.json();
    const criptomonedas = await obtenerCriptomonedas(data.Data);
    llenarSelect(criptomonedas);
  } catch (error) {
    alerta(error);
  }
}

function llenarSelect(monedas) {
  monedas.forEach(moneda => {
    const { Name, FullName } = moneda.CoinInfo;
    const option = document.createElement('option');
    option.textContent = FullName;
    option.value = Name;
    criptomonedasS.appendChild(option);
  });
}

function validarFormulario(e) {
  e.preventDefault();
  const { moneda, criptomoneda } = busqueda;

  if (moneda === '' || criptomoneda === '') {
    alerta('Ambos campos son obligatorios');
    return;
  }

  consultarAPI();
}
function obtenerValorInput(e) {
  busqueda[e.target.name] = e.target.value;
}

function alerta(texto) {

  const alertaPrevia = document.querySelector('.error');
  if (alertaPrevia) {
    return;
  }

  const alerta = document.createElement('div');
  alerta.classList.add('error');
  alerta.textContent = texto;

  formulario.appendChild(alerta);

  setTimeout(() => {
    alerta.remove();
  }, 3000);
}

async function consultarAPI() {
  const { moneda, criptomoneda } = busqueda;
  const URI = `https://min-api.cryptocompare.com/data/pricemultifull?fsyms=${criptomoneda}&tsyms=${moneda}`;

  mostrarSpinner();

  try {
    const response = await fetch(URI);
    const data = await response.json();
    mostrarCotizacion(data.DISPLAY[criptomoneda][moneda]);
  } catch (error) {
    alerta(error);
  }
}

function mostrarCotizacion({ PRICE, LASTUPDATE, HIGHDAY, LOWDAY, CHANGEPCT24HOUR }) {
  limpiarCotizacion();

  const precio = document.createElement('p');
  precio.classList.add('precio');
  precio.innerHTML = `El precio es: <span>${PRICE}</span>`;

  const max = document.createElement('p');
  max.classList.add('precio');
  max.innerHTML = `<p>El precio mas alto del dia es: <span>${HIGHDAY}</span></p>`;

  const min = document.createElement('p');
  min.classList.add('precio');
  min.innerHTML = `<p>El precio mas bajo del dia es: <span>${LOWDAY}</span></p>`;

  const cambio = document.createElement('p');
  cambio.classList.add('precio');
  cambio.innerHTML = `<p>Cambio en las ultimas 24 hora: <span>${CHANGEPCT24HOUR}</span></p>`;

  const actualizacion = document.createElement('p');
  actualizacion.classList.add('precio');
  actualizacion.innerHTML = `<p>Ultima actualizacion: <span>${LASTUPDATE}</span></p>`;



  resultado.appendChild(precio);
  resultado.appendChild(max);
  resultado.appendChild(min);
  resultado.appendChild(cambio);
  resultado.appendChild(actualizacion);
}

function limpiarCotizacion() {
  while (resultado.firstChild) {
    resultado.removeChild(resultado.firstChild);
  }
}

function mostrarSpinner() {
  limpiarCotizacion();
  const spinner = document.createElement('div');
  spinner.classList.add('spinner');
  spinner.innerHTML = `
    <div class="bounce1"></div>
    <div class="bounce2"></div>
    <div class="bounce3"></div>
  `;
  resultado.appendChild(spinner);
}